""" ETC File """
import os
from .string import AlundraString

class Etc:
    """ETC file
    
    The ETC (Exported Text Content?) file stores shared string data for the engine.

    Strings are null terminated and addressed through pointers.
    The store starts with a table of 1024 pointers followed by the string data.

    FR/IT/UK/DE/ES: etc_res.r
    USA: etc_usa.r
    JP: None
    """

    def __init__(self):
        self.strings = []

    def load_from_file(self, filename):
        """Load ETC from file"""
        self.strings = []

        with open(filename, "rb") as source:
            source.seek(0, os.SEEK_END)
            file_size = source.tell()
            source.seek(0, os.SEEK_SET)

            offsets = []
            for _ in range(1024):
                offset = int.from_bytes(source.read(2), byteorder='little')
                offsets.append(offset)

            for offset in offsets:
                if offset >= file_size:
                    # Special case for US, NULL strings points to 0xFFFF
                    self.strings.append((offset, AlundraString(b"")))
                    continue

                source.seek(offset, 0)
                string_bytes = bytearray()
                while True:
                    character = source.read(1)
                    string_bytes.extend(character)
                    if character == b"\00":
                        break

                self.strings.append((offset, AlundraString(string_bytes)))

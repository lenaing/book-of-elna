"""A Simple TIM file viewer."""
import os.path
import re
import sys

from PySide6.QtCore import (
    Qt,
    QElapsedTimer,
)
from PySide6.QtGui import (
    QAction,
    QActionGroup,
    QColor,
    QKeySequence,
    QIcon,
    QImage,
    QPalette,
    QPixmap,
)
from PySide6.QtWidgets import (
    QApplication,
    QCheckBox,
    QDialog,
    QDialogButtonBox,
    QDockWidget,
    QFileDialog,
    QFormLayout,
    QFrame,
    QGroupBox,
    QGraphicsScene,
    QGraphicsView,
    QGridLayout,
    QHBoxLayout,
    QLabel,
    QMainWindow,
    QVBoxLayout,
    QSlider,
    QSpinBox,
    QTableWidget,
    QTableWidgetItem,
    QProgressDialog,
)

from psx.graphics.tim import (
    TIM,
    TIMError,
    TIMOutputFormat,
    TIMPixelMode,
    TIMTransparencyMode,
)

class CLUTEntryBox(QLabel):
    """Widget to dixplay a CLUT Entry Color"""
    def __init__(self, parent, index):
        super().__init__()
        self._parent = parent
        self.index = index
        self.setMinimumSize(14, 14)
        self.setMaximumSize(14, 14)
        self.setFrameStyle(QFrame.Panel | QFrame.Sunken)
        self.setAutoFillBackground(True)

    def mouseReleaseEvent(self, _): # pylint: disable=invalid-name
        """Handle mouse click"""
        self._parent.select_clut_entry(self.index)

class GraphicsScene(QGraphicsScene):
    """Override QGraphicsScene to allow Drag and Drop and Zoom"""

    def __init__(self, parent):
        super().__init__()
        self._parent = parent
        self.transparency = True
        self._pixmap = QPixmap("resources/checked_pattern.png")

    def dragMoveEvent(self, event): # pylint: disable=invalid-name
        """Override QGraphicsScene.dragMoveEvent to allow Drag'n'Drop"""
        if event.mimeData().hasUrls():
            event.acceptProposedAction()

    def dropEvent(self, event): # pylint: disable=invalid-name
        """Handle Drop event"""
        if event.mimeData().hasUrls():
            self._parent.handle_drop(event.mimeData().urls())

    def wheelEvent(self, event): # pylint: disable=invalid-name
        """Handle MouseWheel"""
        if event.modifiers() & Qt.ControlModifier:
            if event.delta() < 0:
                self._parent.zoom_out()
            else:
                self._parent.zoom_in()

    def drawBackground(self, painter, _): # pylint: disable=invalid-name
        """Override QGraphicsScene.drawBackground to display transparency"""
        if not self.transparency:
            return
        painter.save()
        painter.resetTransform()
        painter.drawTiledPixmap(self._parent.view.rect(), self._pixmap)
        painter.restore()

class GraphicsView(QGraphicsView):
    """Override QGraphicsView to allow mouse scrolling"""

    def mousePressEvent(self, event): # pylint: disable=invalid-name
        """Handle mouse pressed"""
        self._mouse_pressed = True
        self._pan_x = event.position().x()
        self._pan_y = event.position().y()

    def mouseReleaseEvent(self, _): # pylint: disable=invalid-name
        """Handle mouse pressed"""
        self._mouse_pressed = False

    def mouseMoveEvent(self, event): # pylint: disable=invalid-name
        """Handle mouse moved"""
        if self._mouse_pressed:
            h_value = self.horizontalScrollBar().value() - (event.position().x() - self._pan_x)
            v_value = self.verticalScrollBar().value() - (event.position().y() - self._pan_y)
            self.horizontalScrollBar().setValue(h_value)
            self.verticalScrollBar().setValue(v_value)
            self._pan_x = event.position().x()
            self._pan_y = event.position().y()

class MainWindow(QMainWindow):
    """Main Window"""

    def __init__(self):
        super().__init__()

        self._loaded_file = None
        self._raw_scan_index = None
        self._raw_scan_result = []
        self._tim = None
        self._tim_image = None
        self._clut_set = 0
        self._clut_entry_box_index = 0
        self._transparency = TIMTransparencyMode.ENABLED

        self.setWindowTitle("TIM Viewer")
        self.setWindowIcon(QIcon("resources/book.png"))

        available_geometry = self.screen().availableGeometry()
        self.resize(available_geometry.width() / 3, available_geometry.height() / 2)

        self.create_actions()
        self.create_menus()
        self.create_view()
        self.create_image_info_dock()
        self.create_clut_info_dock()
        self.create_raw_scan_dock()

        self.statusBar().showMessage("Ready")


    def create_actions(self):
        """Create application actions."""
        actions = {}

        # File related actions
        actions["open_file"] = QAction(
            QIcon.fromTheme('document-open'),
            "&Open...",
            self,
            shortcut=QKeySequence.Open,
            statusTip="Open a TIM file",
            triggered=self.open_file
        )
        actions["open_raw_file"] = QAction(
            QIcon.fromTheme('document-open'),
            "Open RAW file...",
            self,
            statusTip="Open a RAW file",
            triggered=self.open_raw_file
        )

        actions["save_file"] = QAction(
            QIcon.fromTheme('document-save'),
            "E&xport ...",
            self,
            shortcut="Ctrl+E",
            statusTip="Export the image",
            triggered=self.save_file,
            enabled=False
        )

        # Transparency related actions
        actions["transparency_disabled"] = QAction(
            QIcon.fromTheme('weather-overcast'),
            "&Disabled",
            self,
            statusTip="Disable Transparency",
            triggered=self.transparency_disabled,
            checkable=True
        )
        actions["transparency_no_semi"] = QAction(
            QIcon.fromTheme('weather-few-clouds'),
            "&Ignore Semi Transparency",
            self,
            statusTip="Ignore Semi TransParency color flag",
            triggered=self.transparency_no_semi,
            checkable=True
        )
        actions["transparency_enabled"] = QAction(
            QIcon.fromTheme('weather-clear'),
            "&Enabled",
            self,
            statusTip="Enable Transparency",
            triggered=self.transparency_enabled,
            checkable=True,
        )
        actions["transparency_enabled"].setChecked(True)

        # Zoom related actions
        actions["zoom_in"] = QAction(
            QIcon.fromTheme('zoom-in'),
            "Zoom &In",
            self,
            shortcut=QKeySequence.ZoomIn,
            statusTip="Zoom view in",
            triggered=self.zoom_in
        )
        actions["zoom_out"] = QAction(
            QIcon.fromTheme('zoom-out'),
            "Zoom &Out",
            self,
            shortcut=QKeySequence.ZoomOut,
            statusTip="Zoom view out",
            triggered=self.zoom_out
        )
        actions["zoom_original"] = QAction(
            QIcon.fromTheme('zoom-original'),
            "&Actual Size",
            self,
            shortcut="Ctrl+0",
            statusTip="Zoom to original view size",
            triggered=self.zoom_original
        )

        # General related actions
        actions["quit"] = QAction(
            QIcon.fromTheme('application-exit'),
            "&Quit",
            self,
            shortcut=QKeySequence.Quit,
            statusTip="Quit TIM Viewer",
            triggered=self.close
        )

        actions["about"] = QAction(
            QIcon.fromTheme('application-about'),
            "&About",
            self,
            statusTip="About TIM Viewer",
            triggered=self.about
        )

        self._actions = actions

    def create_view(self):
        """Create central view"""
        self._scene = GraphicsScene(self)
        view = GraphicsView()
        view.setScene(self._scene)
        view.setViewportUpdateMode(QGraphicsView.FullViewportUpdate)
        self.view = view
        self.setCentralWidget(view)

    def create_image_info_dock(self):
        """Create image info dock"""
        self._image_size            = QLabel()
        self._image_x               = QLabel()
        self._image_y               = QLabel()
        self._image_width           = QLabel()
        self._image_height          = QLabel()
        self._image_bpp             = QLabel()
        self._image_rendered_width  = QLabel()

        form = QFormLayout()
        form.addRow("Size:",             self._image_size)
        form.addRow("X:",                self._image_x)
        form.addRow("Y:",                self._image_y)
        form.addRow("Width:",            self._image_width)
        form.addRow("Height:",           self._image_height)
        form.addRow("Color Depth:",      self._image_bpp)
        form.addRow("Rendered Width:",   self._image_rendered_width)

        image_infogroup = QGroupBox()
        image_infogroup.setLayout(form)
        image_infogroup.setMaximumHeight(175)

        dock = QDockWidget("Image Informations", self)
        dock.setFeatures(QDockWidget.DockWidgetMovable | QDockWidget.DockWidgetFloatable)
        dock.setWidget(image_infogroup)
        self.addDockWidget(Qt.LeftDockWidgetArea, dock)

    def create_clut_info_dock(self):
        """Create CLUT info dock"""
        self._clut_size         = QLabel()
        self._clut_x            = QLabel()
        self._clut_y            = QLabel()
        self._clut_width        = QLabel()
        self._clut_height       = QLabel()
        self._clut_set_spinbox  = QSpinBox()
        self._clut_set_slider   = QSlider(Qt.Horizontal)

        self._clut_set_spinbox.setMinimum(1)
        self._clut_set_slider.setMinimum(1)
        self._clut_set_slider.setTickInterval(1)
        self._clut_set_slider.setTickPosition(QSlider.TicksBelow)
        self._clut_set_slider.valueChanged.connect(self._clut_set_spinbox.setValue)
        self._clut_set_spinbox.valueChanged.connect(self._clut_set_slider.setValue)
        self._clut_set_spinbox.valueChanged.connect(self.select_clut_set)

        form_layout = QFormLayout()
        form_layout.addRow("Size:",     self._clut_size)
        form_layout.addRow("X:",        self._clut_x)
        form_layout.addRow("Y:",        self._clut_y)
        form_layout.addRow("Width:",    self._clut_width)
        form_layout.addRow("Height:",   self._clut_height)
        form_layout.addRow("Set:",      self._clut_set_spinbox)
        form_layout.addRow("",          self._clut_set_slider)

        self._clut_entry_boxes = []
        for idx in range(0,256):
            self._clut_entry_boxes.append(CLUTEntryBox(self, idx))

        vbox = QVBoxLayout()
        vbox.setSpacing(2)
        for row in range(0,16):
            hbox = QHBoxLayout()
            hbox.setSpacing(2)
            for col in range(0,16):
                hbox.addWidget(self._clut_entry_boxes[col + row*16])
            hbox.addStretch()
            vbox.addLayout(hbox)
        vbox.addStretch()

        set_infogroup = QGroupBox("Current Set")
        set_infogroup.setLayout(vbox)

        grid = QGridLayout()

        self._current_clut_entry = {}
        index_label = QLabel("0")
        grid.addWidget(QLabel("Index:"), 0, 0)
        grid.addWidget(index_label, 0, 1)
        self._current_clut_entry["index"] = index_label
        for idx, color in enumerate(["red", "green", "blue"]):
            spinbox = QSpinBox()
            spinbox.setReadOnly(True)
            spinbox.setMaximum(255)
            slider = QSlider(Qt.Horizontal)
            slider.setEnabled(False)
            slider.setTickPosition(QSlider.TicksBelow)
            slider.setMaximum(255)
            spinbox.valueChanged.connect(slider.setValue)
            slider.valueChanged.connect(spinbox.setValue)

            grid.addWidget(QLabel(f"{color.capitalize()}:"), idx + 1, 0)
            grid.addWidget(slider, idx + 1, 1)
            grid.addWidget(spinbox, idx + 1, 2)
            self._current_clut_entry[color] = spinbox

        transparent_checkbox = QCheckBox("Transparent")
        transparent_checkbox.setEnabled(False)
        grid.addWidget(transparent_checkbox, 4, 0, 1, 0)
        self._current_clut_entry["transparent"] = transparent_checkbox

        color_label = QLabel()
        color_label.setFrameStyle(QFrame.Panel | QFrame.Sunken)
        color_label.setAutoFillBackground(True)
        grid.addWidget(color_label, 4, 2)
        self._current_clut_entry["color"] = color_label

        current_entry = QGroupBox("Selected CLUT Entry")
        current_entry.setLayout(grid)

        vbox = QVBoxLayout()
        vbox.addLayout(form_layout)
        vbox.addWidget(set_infogroup)
        vbox.addWidget(current_entry)
        vbox.addStretch()

        clut_infogroup = QGroupBox()
        clut_infogroup.setLayout(vbox)
        clut_infogroup.setVisible(False)

        dock = QDockWidget("CLUT Informations", self)
        dock.setFeatures(QDockWidget.DockWidgetMovable | QDockWidget.DockWidgetFloatable)
        dock.setVisible(False)
        dock.setWidget(clut_infogroup)
        self.clut_info_dock = dock
        self.addDockWidget(Qt.RightDockWidgetArea, self.clut_info_dock)

    def create_raw_scan_dock(self):
        """Create RAW Scan dock"""
        table = QTableWidget()
        table.setRowCount(0)
        headers = ["Offset", "Resolution", "Color Depth"]
        table.setColumnCount(len(headers))
        table.setHorizontalHeaderLabels(headers)
        table.horizontalHeader().setDefaultAlignment(Qt.AlignLeft)
        table.horizontalHeader().setStretchLastSection(True)
        table.cellClicked.connect(self.load_raw_scan_result)
        self._raw_scan_table = table

        dock = QDockWidget("RAW Scan Results", self)
        dock.setFeatures(QDockWidget.DockWidgetMovable | QDockWidget.DockWidgetFloatable)
        dock.setVisible(False)
        dock.setWidget(table)
        self.raw_scan_dock = dock
        self.addDockWidget(Qt.LeftDockWidgetArea, dock)

    def create_menus(self):
        """Create UI Menus"""
        file_menu = self.menuBar().addMenu("&File")
        file_menu.addAction(self._actions["open_file"])
        file_menu.addAction(self._actions["open_raw_file"])
        file_menu.addSeparator()
        file_menu.addAction(self._actions["save_file"])
        file_menu.addSeparator()
        file_menu.addAction(self._actions["quit"])

        view_menu = self.menuBar().addMenu("&View")

        transparency_menu = view_menu.addMenu("&Transparency")
        action_group = QActionGroup(self, exclusive=True)
        action_group.addAction(self._actions["transparency_disabled"])
        transparency_menu.addAction(self._actions["transparency_disabled"])
        action_group.addAction(self._actions["transparency_no_semi"])
        transparency_menu.addAction(self._actions["transparency_no_semi"])
        action_group.addAction(self._actions["transparency_enabled"])
        transparency_menu.addAction(self._actions["transparency_enabled"])

        zoom_menu = view_menu.addMenu("&Zoom")
        zoom_menu.addAction(self._actions["zoom_in"])
        zoom_menu.addAction(self._actions["zoom_out"])
        zoom_menu.addAction(self._actions["zoom_original"])

        help_menu = self.menuBar().addMenu('&Help')
        help_menu.addAction(self._actions["about"])

    def about(self):
        """Show About Dialog"""
        about_dialog = QDialog(self)
        about_dialog.setWindowTitle("About TIM Viewer")

        button_box = QDialogButtonBox(QDialogButtonBox.Ok)
        button_box.accepted.connect(about_dialog.close)

        v_layout = QVBoxLayout(about_dialog)
        v_layout.addWidget(QLabel("TIM Viewer version 0.1.0"))
        v_layout.addWidget(QLabel("Lenain <lenaing@gmail.com>"))
        v_layout.addWidget(button_box)
        about_dialog.setLayout(v_layout)
        about_dialog.exec()

    def open_file(self):
        """Let user select a file and load it"""
        result = QFileDialog.getOpenFileName(
            self,
            self.tr("Open TIM file"),
            filter=self.tr("TIM Files (*.tim)")
        )
        if result:
            self._loaded_file = result[0]
            self.load_tim_from_file(self._loaded_file)

    def open_raw_file(self):
        """Let user select a file and load it"""
        result = QFileDialog.getOpenFileName(
            self,
            self.tr("Open RAW file"),
            filter=self.tr("RAW Files (*.*)")
        )
        if result:
            self._loaded_file = result[0]
            self.scan_raw_file(self._loaded_file)

    def save_file(self):
        """Let user select a file to save it"""
        save_dialog = QFileDialog()
        save_dialog.setFileMode(QFileDialog.AnyFile)
        save_dialog.setAcceptMode(QFileDialog.AcceptSave)
        save_dialog.setDirectory(".")
        save_dialog.setWindowTitle(self.tr("Export file"))
        save_dialog.setNameFilter(self.tr("Export to PNG (*.png)"))

        directory = os.path.dirname(self._loaded_file)
        filename = os.path.splitext(os.path.basename(self._loaded_file))[0]

        if self._raw_scan_index is not None:
            filename += f"_0x{self._raw_scan_result[self._raw_scan_index][0]:X}"

        if self._tim.clut and self._tim.clut.height > 1:
            filename += f"_{self._clut_set}"

        filename += ".png"

        save_dialog.selectFile(os.path.join(directory, filename))
        ret = save_dialog.exec()
        if ret != QDialog.Accepted:
            return

        filename = save_dialog.selectedFiles()[0]
        export_filter = save_dialog.selectedNameFilter()

        file_extension = os.path.splitext(filename)[1]
        filter_extension = re.search(r'\(\*(?P<ext>.[a-z]+)\)', export_filter).group('ext')

        if file_extension != filter_extension:
            filename += filter_extension

        if filter_extension in [".png"]:
            self._tim_image.save(filename, None, 100)

    def zoom_in(self):
        """Zoom in"""
        self.view.scale(2, 2)

    def zoom_out(self):
        """Zoom out"""
        self.view.scale(0.5, 0.5)

    def zoom_original(self):
        """Reset zoom"""
        self.view.resetTransform()

    def transparency_disabled(self):
        """Disable transparency"""
        self._transparency = TIMTransparencyMode.DISABLED
        self._scene.transparency = False
        self.render_tim()

    def transparency_no_semi(self):
        """Do not respect STP flags"""
        self._transparency = TIMTransparencyMode.NO_SEMI_TRANSPARENCY
        self._scene.transparency = True
        self.render_tim()


    def transparency_enabled(self):
        """Enable transparency"""
        self._transparency = TIMTransparencyMode.ENABLED
        self._scene.transparency = True
        self.render_tim()

    def handle_drop(self, files):
        """Handle file drop"""
        self._loaded_file = files[0].toLocalFile()
        self.scan_raw_file(self._loaded_file)

    def reset_ui(self):
        """Reset UI"""
        self._actions["save_file"].setEnabled(False)
        self._raw_scan_index = None
        self.raw_scan_dock.setVisible(False)
        self.clut_info_dock.setVisible(False)
        self._image_size.setText("")
        self._image_x.setText("")
        self._image_y.setText("")
        self._image_width.setText("")
        self._image_height.setText("")
        self._image_bpp.setText("")
        self._image_rendered_width.setText("")
        self._scene.clear()

    def load_tim_from_file(self, filename):
        """Load TIM file"""
        if not filename:
            return
        self.reset_ui()
        try:
            self._tim = TIM.from_file(filename)
            self.statusBar().showMessage(f"Successfully loaded '{filename}'.")
        except TIMError as err:
            self.statusBar().showMessage(f"Failed to load '{filename}': {err}.")
            return
        self.setWindowTitle(f"{os.path.basename(filename)} - TIM Viewer")
        self.load_tim()

    def scan_raw_file(self, filename):
        """Scan RAW file"""
        if not filename:
            return
        self.reset_ui()

        images_found = []

        self.statusBar().showMessage(f"Started scanning '{filename}' ...")
        timer = QElapsedTimer()
        timer.start()
        with open(filename, "rb") as source:
            source.seek(0, os.SEEK_END)
            file_size = source.tell()
            source.seek(0, os.SEEK_SET)
            one_percent = file_size // 100
            if not one_percent:
                one_percent = 1

            data = source.read()
            offset = 0

            progress = QProgressDialog("", "Cancel", 0, file_size, self)
            progress.setWindowTitle("RAW Scan")
            progress.setModal(True)
            progress.forceShow()

            while offset < file_size:
                cur_pos = offset
                if data[offset:offset+4] == b'\x10\x00\x00\x00':
                    try:
                        (new_offset, tim) = TIM.from_bytes(data, offset)
                        offset = new_offset
                        images_found.append((cur_pos, tim))
                    except TIMError:
                        # Failed to load TIM with data, reset to next byte after potential match
                        offset = cur_pos + 1
                else:
                    offset += 1

                if offset % one_percent == 0:
                    progress.setLabelText(
                        f"Scanning '{filename}'\n" \
                        f"{offset} / {file_size}\n" \
                        f"{len(images_found)} images found."
                    )
                    progress.setValue(offset)

                if progress.wasCanceled():
                    break

            if progress.isVisible() and not progress.value() == file_size:
                progress.setValue(file_size)

        self._raw_scan_result = images_found

        self.statusBar().showMessage(
            f"Scanned '{filename}' in {timer.elapsed()}ms. " \
            f"Found {len(images_found)} images."
        )
        self.setWindowTitle(f"{os.path.basename(filename)} - TIM Viewer")

        self._raw_scan_table.clearContents()
        self._raw_scan_table.setRowCount(len(images_found))
        for idx, image in enumerate(images_found):
            (offset, tim) = image
            offset_item = QTableWidgetItem(f"0x{offset:X}")
            offset_item.setFlags(Qt.ItemIsEnabled)
            self._raw_scan_table.setItem(idx, 0, offset_item)
            width_item = QTableWidgetItem(f"{tim.height} x {tim.render_width()}")
            width_item.setFlags(Qt.ItemIsEnabled)
            self._raw_scan_table.setItem(idx, 1, width_item)
            bpp_item = QTableWidgetItem(f"{tim.pixel_mode}")
            bpp_item.setFlags(Qt.ItemIsEnabled)
            self._raw_scan_table.setItem(idx, 2, bpp_item)

        if images_found:
            self.load_raw_scan_result(0)

        self.raw_scan_dock.setVisible(True)

    def load_raw_scan_result(self, index):
        """Load a given RAW Scan result"""
        self._raw_scan_index = index
        (_, tim) = self._raw_scan_result[self._raw_scan_index]
        self._tim = tim
        self.load_tim()

    def load_tim(self):
        """Load current TIM"""
        self._clut_set = 0
        self._scene.clear()
        self.clut_info_dock.setVisible(False)
        self._actions["save_file"].setEnabled(True)

        self._image_size.setText(str(self._tim.size))
        self._image_x.setText(str(self._tim.pos_x))
        self._image_y.setText(str(self._tim.pos_y))
        self._image_height.setText(str(self._tim.height))
        self._image_width.setText(str(self._tim.width))
        self._image_rendered_width.setText(str(self._tim.render_width()))
        self._image_bpp.setText(str(self._tim.pixel_mode))

        if self._tim.clut:
            self._clut_size.setText(str(self._tim.clut.size))
            self._clut_x.setText(str(self._tim.clut.pos_x))
            self._clut_y.setText(str(self._tim.clut.pos_y))
            self._clut_height.setText(str(self._tim.clut.height))
            self._clut_width.setText(str(self._tim.clut.width))
            clut_sets = self._tim.clut.height

            self._clut_set_spinbox.setValue(1)
            self._clut_set_spinbox.setMaximum(clut_sets)
            self._clut_set_slider.setMaximum(clut_sets)

            # Show only CLUT Entry Boxes matching TIM Pixel Mode
            visibility = self._tim.pixel_mode == TIMPixelMode.BIT8_CLUT
            for row in range(1,16):
                for col in range(0, 16):
                    self._clut_entry_boxes[col + row*16].setVisible(visibility)

            self.clut_info_dock.setVisible(True)

        self.render_tim()

    def select_clut_set(self, value):
        """Select a new CLUT set"""
        self._clut_set = value-1
        self.render_tim()

    def render_tim(self):
        """Render current TIM with selected CLUT set"""
        if not self._tim:
            return

        tim_data   = self._tim.to_format(
            TIMOutputFormat.RGBA8888,
            self._clut_set,
            transparency=self._transparency
        )
        tim_width  = self._tim.render_width()
        tim_height = self._tim.height
        self._tim_image = QImage(tim_data, tim_width, tim_height, QImage.Format_RGBA8888)
        self._scene.clear()
        self._scene.addPixmap(QPixmap.fromImage(self._tim_image))
        self._scene.setSceneRect(0, 0, self._tim_image.width(), self._tim_image.height())

        # Update CLUT Entry Boxes colors
        if self._tim.clut:
            max_row = 1
            if self._tim.pixel_mode == TIMPixelMode.BIT8_CLUT:
                max_row = 16

            for row in range(0, max_row):
                for col in range(0, 16):
                    index = col + row * 16
                    clut_entry = self._tim.clut.entry_at(index, self._clut_set)
                    (red, green, blue) = clut_entry.to_rgb()
                    pal = QPalette()
                    pal.setColor(QPalette.Window, QColor(red, green, blue))
                    self._clut_entry_boxes[index].setPalette(pal)

            self.select_clut_entry(0)

    def select_clut_entry(self, index):
        """Select a CLUT entry and display its informations"""
        old_index = self._clut_entry_box_index
        self._clut_entry_box_index = index

        # Reset Old Box style
        self._clut_entry_boxes[old_index].setLineWidth(1)

        # Set Selected Box style and load CLUT entry informations
        self._clut_entry_boxes[index].setLineWidth(3)
        clut_entry = self._tim.clut.entry_at(index, self._clut_set)
        clut_entry_index = index + self._clut_set * self._tim.clut.width
        (red, green, blue) = clut_entry.to_rgb()
        pal = QPalette()
        pal.setColor(QPalette.Window, QColor(red, green, blue))
        self._current_clut_entry["index"].setText(str(clut_entry_index))
        self._current_clut_entry["red"].setValue(red)
        self._current_clut_entry["green"].setValue(green)
        self._current_clut_entry["blue"].setValue(blue)
        self._current_clut_entry["transparent"].setChecked(clut_entry.transparency)
        self._current_clut_entry["color"].setPalette(pal)

if __name__ == '__main__':
    app = QApplication()
    MainWindow().show()
    sys.exit(app.exec())

# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [0.1.0-alpha.1] - 2023-08-26

### Added

- This changelog.
- DWTFYWTPL License.
- BoE: Loading and displaying as text the External Text Content (`etc*.r`) file's strings.
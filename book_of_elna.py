"""An Alundra Game Editor."""
import logging
import sys

from PySide6.QtCore import (
    Qt,
    QSettings,
)
from PySide6.QtGui import (
    QAction,
    QKeySequence,
    QIcon,
)
from PySide6.QtWidgets import (
    QApplication,
    QDockWidget,
    QDialog,
    QDialogButtonBox,
    QFormLayout,
    QFileDialog,
    QLabel,
    QLineEdit,
    QMainWindow,
    QTableWidget,
    QTableWidgetItem,
    QVBoxLayout,
)
from alundra.text.etc import Etc

BOOK_VERSION = '0.1.0-alpha.1'

logging.basicConfig(
    level=logging.INFO,
    format= '[%(asctime)s] {%(pathname)s:%(lineno)d} %(levelname)s - %(message)s',
    datefmt='%H:%M:%S'
 )

class BookOfElnaState:
    """Application State"""
    def __init__(self):
        self.loaded = False
        self.settings = QSettings("book_of_elna.ini", QSettings.IniFormat)
        self.etc_path = self.settings.value("editor/etc")
        self.etc = None

class MainWindow(QMainWindow):
    """Main Window"""

    def __init__(self):
        super().__init__()

        self.state = BookOfElnaState()

        self.setWindowTitle("Book of Elna")
        self.setWindowIcon(QIcon("resources/book.png"))

        available_geometry = self.screen().availableGeometry()
        self.resize(available_geometry.width() / 3, available_geometry.height() / 2)

        self.create_actions()
        self.create_menus()
        self.create_etc_strings_dock()

        self.statusBar().showMessage("Ready")

    def create_actions(self):
        """Create application actions."""
        actions = {}

        # File related actions
        actions["open"] = QAction(
            QIcon.fromTheme('document-open'),
            "&Open...",
            self,
            shortcut=QKeySequence.Open,
            statusTip="Open data files",
            triggered=self.open
        )

        # General related actions
        actions["quit"] = QAction(
            QIcon.fromTheme('application-exit'),
            "&Quit",
            self,
            shortcut=QKeySequence.Quit,
            statusTip="Quit Application",
            triggered=self.close
        )

        actions["about"] = QAction(
            QIcon.fromTheme('application-about'),
            "&About",
            self,
            statusTip="About the Book of Elna",
            triggered=self.about
        )

        self._actions = actions

    def create_menus(self):
        """Create UI Menus"""
        file_menu = self.menuBar().addMenu("&File")
        file_menu.addAction(self._actions["open"])
        file_menu.addSeparator()
        file_menu.addAction(self._actions["quit"])

        help_menu = self.menuBar().addMenu('&Help')
        help_menu.addAction(self._actions["about"])

    def create_etc_strings_dock(self):
        """Create etc strings dock"""
        table = QTableWidget()
        table.setRowCount(0)
        table_columns = ["Offset", "String"]
        table.setColumnCount(len(table_columns))
        table.setHorizontalHeaderLabels(table_columns)
        table.horizontalHeader().setDefaultAlignment(Qt.AlignLeft)
        table.horizontalHeader().setStretchLastSection(True)
        self._etc_strings_table = table

        dock = QDockWidget("ETC Strings", self)
        dock.setFeatures(QDockWidget.DockWidgetMovable | QDockWidget.DockWidgetFloatable)
        dock.setWidget(table)
        self.addDockWidget(Qt.LeftDockWidgetArea, dock)

    def about(self):
        """Show About Dialog"""
        about_dialog = QDialog(self)
        about_dialog.setWindowTitle("About the Book of Elna")

        button_box = QDialogButtonBox(QDialogButtonBox.Ok)
        button_box.accepted.connect(about_dialog.close)

        v_layout = QVBoxLayout(about_dialog)
        v_layout.addWidget(QLabel(f"Book of Elna version {BOOK_VERSION}"))
        v_layout.addWidget(QLabel("Lenain <lenaing@gmail.com>"))
        v_layout.addWidget(button_box)
        about_dialog.setLayout(v_layout)
        about_dialog.exec()

    def open(self):
        """Open data files"""
        open_form = OpenDialog(self)
        res = open_form.exec()
        if res == QDialog.Accepted:
            self.statusBar().showMessage("Loading ETC File...")
            etc_path = self.state.settings.value("editor/etc")
            etc = Etc()
            etc.load_from_file(etc_path)
            self.state.etc = etc
            self.reload_etc_string_table()
            self.statusBar().showMessage(f"Successfully loaded {len(etc.strings)} string(s).")

    def reload_etc_string_table(self):
        """Reload General String table"""
        etc = self.state.etc
        table = self._etc_strings_table
        table.clearContents()
        table.setRowCount(len(etc.strings))

        item_flags = Qt.ItemIsEnabled | Qt.ItemIsSelectable
        for idx, (offset, string) in enumerate(etc.strings):
            string_item = QTableWidgetItem(str(string))
            string_item.setFlags(item_flags)
            offset_item = QTableWidgetItem(f"0x{offset:X}")
            offset_item.setFlags(item_flags)
            table.setItem(idx, 0, offset_item)
            table.setItem(idx, 1, string_item)

class OpenDialog(QDialog):
    """Open dialog"""

    def __init__(self, parent):
        super().__init__(parent)
        self.settings = parent.state.settings
        self.setWindowTitle("Open")

        edit = QLineEdit()
        edit.setText(self.settings.value("editor/etc"))
        self._edit = edit

        edit_action = edit.addAction(QIcon.fromTheme('document-open'), QLineEdit.TrailingPosition)
        edit_action.triggered.connect(self.open_etc)

        form = QFormLayout()
        form.addRow(self.tr("ETC File:"), edit)

        box = QDialogButtonBox(QDialogButtonBox.Open | QDialogButtonBox.Cancel)
        box.accepted.connect(self.open)
        box.rejected.connect(self.reject)

        vbox = QVBoxLayout(self)
        vbox.addLayout(form)
        vbox.addWidget(box)
        self.setLayout(vbox)

    def open_etc(self):
        """Let user select an External Text Content file"""
        result = QFileDialog.getOpenFileName(
            self,
            self.tr("Open ETC file"),
            filter=self.tr("*.r (*.r)")
        )
        if result:
            self._edit.setText(result[0])

    def open(self):
        """Update configuration with given configuration"""
        self.settings.setValue("editor/etc", self._edit.text())
        self.accept()

if __name__ == '__main__':
    app = QApplication()
    main_window = MainWindow()
    main_window.show()
    sys.exit(app.exec())

"""Alundra String"""

import logging

logger = logging.getLogger(__name__)

class AlundraString:
    """An Alundra string"""

    def __init__(self, string_bytes):
        self.bytes = string_bytes

    # US and UK are kind of ASCII
    # DE, ES, FR, IT are CP1252
    def __str__(self):
        left_brace_extended = False
        right_brace_extended = False
        non_extended = False
        unknown_nonascii = False
        string_bytes = []

        # Iterate over all source string's characters except the null terminating character
        for character in self.bytes[:-1]:

            if left_brace_extended or right_brace_extended:
                if right_brace_extended:
                    if character > 0x6F:
                        logger.warning("Found extended character over 255: %X", character)
                    right_brace_extended = False
                    character += 0x90
                if left_brace_extended:
                    if character > 0xAF:
                        logger.warning("Found extended character over 255: %X", character)
                    left_brace_extended = False
                    character += 0x50
            else:
                if character < 0x20:
                    if character == 0x0A:
                        # Only in UK version, some newline are doubled, skip them
                        continue
                    if character == 0x1A:
                        string_bytes += list("[[SQUARE]]".encode())
                        continue
                    if character == 0x1C:
                        string_bytes += list("[[CIRCLE]]".encode())
                        continue

                    logger.warning("Found unknown character lower than 0x20: %X", character)
                    unknown_nonascii = True

                if character > 0x7F:
                    # Skip non prefix-extended ASCII Character.
                    # This happens :
                    # * ES: Only once for an Inverted question mark 0xBF.
                    # * US: Multiple times in the same buggy and propably unused string.
                    logger.warning("Found non extended character lower over 0x7F: %X", character)
                    non_extended = True
                    continue

            if character == 0x7B:
                # ES, IT
                left_brace_extended = True
            elif character == 0x7D:
                # DE, ES, FR, IT
                right_brace_extended = True
            else:
                string_bytes.append(character)
        if non_extended or unknown_nonascii:
            logger.warning("In: %s", self.bytes)
        return bytes(string_bytes).decode("cp1252")

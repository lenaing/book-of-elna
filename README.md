# Book of Elna

This repository will hold an editor for the PSX Game [Alundra](https://www.igdb.com/games/alundra) and other PSX tools.

## Requirements

Install requirements:

```
sudo apt install libxcb-cursor0
python3 -m venv .venv
source .venv/bin/activate
pip install -r requirements.txt
```

## Book of Elna

Open the Book:

```
python book_of_elna.py
```

### Features

* External Text Content
    * Load
    * Display as text

## Tools

### TIM Viewer

Launch TIM viewer:

```
python -m tools.tim_viewer
```

#### Features

* Pixel Modes:
    * 4-bit CLUT
    * 8-bit CLUT
    * 15-bit Direct
    * 24-bit Direct
* Multiple CLUT sets
* Zoom in/out
* Drag and Drop file loading
* RAW File Scanning
* Transparency
* Export to PNG

"""TIM Screen Image Data"""
import os.path
from enum import Enum, auto

CLUT_HEADER_SIZE = 12
PIXEL_DATA_HEADER_SIZE = 12

class TIMPixelMode(Enum):
    """TIM Pixel mode (Bit length)"""
    BIT4_CLUT = 0
    BIT8_CLUT = 1
    BIT15_DIRECT = 2
    BIT24_DIRECT = 3
    MIXED = 4

    def __str__(self):
        if self.value == 0:
            return '4 Bit Image'
        if self.value == 1:
            return '8 Bit Image'
        if self.value == 2:
            return '16 Bit Image'
        if self.value == 3:
            return '24 Bit Image'
        return 'Mixed Image'

class TIMOutputFormat(Enum):
    """TIM Pixel mode (Bit length)"""
    RGB888 = auto()
    RGBA8888 = auto()
    BGR888 = auto()

class TIMTransparencyMode(Enum):
    """Transparency mode"""
    DISABLED = auto()
    NO_SEMI_TRANSPARENCY = auto()
    ENABLED = auto()

class TIM:
    """TIM Screen Image Data

    The TIM file covers standard images handled by the PlayStation unit, and can be transferred
    directly to its VRAM. It can be used commonly as sprite patterns and 3D texture mapping
    materials.
    The following are the image data modes (color counts) handled by the PlayStation unit.

    * 4-bit CLUT
    * 8-bit CLUT
    * 16-bit Direct color
    * 24-bit Direct color

    The VRAM supported by the PlayStation unit is based on 16 bits. Thus, only 16- and 24-bit data
    can be transferred directly to the frame buffer for display. Use as sprite pattern or polygon
    texture mapping data allows the selection of any of 4-bit, 8-bit and 16-bit modes.

    TIM files have a file header (ID) at the top and consist of several different blocks.

    | 31             00 |
    +-------------------+
    |        ID         |
    +-------------------+
    |       FLAG        |
    +-------------------+
    |       CLUT        |
    |        ..         |
    |        ..         |
    +-------------------+
    |      PIXELS       |
    |        ..         |
    |        ..         |
    +-------------------+

    Each data item is a string of 32-bit binary data. The data is Little Endian, so in an item of
    data containing several bytes, the bottom byte comes first (holds the lowest address).

    ID
    ----------

    The file ID is composed of one word, having the following bit configuration.

    | 31              16 | 15          08 | 07          00 |
    +------------------------------------------------------+
    |      Reserved      |   Version No.  |       ID       |
    +------------------------------------------------------+

    Bits 0 - 7:     ID value is 0x10.
    Bits 8 - 15:    Version number. Value is 0x00.

    Flag
    ----------

    Flags are 32-bit data containing information concerning the file structure. When a single TIM
    data file contains numerous sprites and texture data, the value of PMODE is 4 (mixed), since
    data of multiple types is intermingled.

    | 31                                   04 | 03 | 02 00 |
    +------------------------------------------------------+
    |                 Reserved                | CF | PMODE |
    +------------------------------------------------------+

    Bits 0 - 3 (PMODE): Pixel mode (Bit length)
        0:  4-bit CLUT
        1:  8-bit CLUT
        2:  15-bit direct
        3:  24-bit direct
        4:  Mixed

    Bit 4 (CF):         Whether there is a CLUT or not
        0:  No CLUT section
        1:  Has CLUT section

    CLUT
    ----------

    Cf CLUT class.

    PIXELS
    ----------

    Pixel data is the substance of the image data. The frame buffer of the PlayStation system has a
    16-bit structure, so image data is broken up into 16-bit units.

    | 31             00 |
    +-------------------+
    |       bnum        |
    +-------------------+
    |   DY    |  DX     |
    +-------------------+
    |    H    |   W     |
    +-------------------+
    |  DATA1  | DATA0   |
    +-------------------+
    |        ...        |
    |        ...        |
    +-------------------+
    |  DATAn  | DATAn-1 |
    +-------------------+

    bnum:       Data length of the pixel data block. Units: bytes. Includes the 4 bytes of bnum.
    DX:         x coordinate in frame buffer
    DY:         y coordinate in frame buffer
    H:          Size of data in vertical direction
    W:          Size of data in horizontal direction (in 16-bit units)
    DATA 1~N:   Frame buffer data (16 bits per entry)

    The structure of one item of frame buffer data (16 bits) varies according to the image data
    mode.

    Care is needed when handling the size of the pixel data within the TIM data. The W value
    (horizontal width) is in 16-pixel units, so in 4-bit or 8-bit mode it will be, respectively,
    1/4 or 1/2 of the actual image size. Accordingly, the horizontal width of an image size in
    4-bit mode has to be a multiple of 4, and an image size in 8-bit mode has to be an even number.

    PIXEL DATA
    ----------

    In 4-bit mode:

        | 15       12 | 11       08 | 07       04 | 03       00 |
        +-------------------------------------------------------+
        |    Pix 3    |    Pix 2    |    Pix 1    |    Pix 0    |
        +-------------------------------------------------------+

        Pix 0-3: Pixel Value (CLUT No.)
        The order on the screen is Pix 0, 1, 2, 3, from the LSB side.

    In 8-bit mode:

        | 15                     08 | 07                     00 |
        +-------------------------------------------------------+
        |           Pix 1           |           Pix 0           |
        +-------------------------------------------------------+

        Pix 0-1: Pixel Value (CLUT No.)
        The order on the screen is Pix 0, 1 from the LSB side.

    In 16-bit mode:

        | 15 | 14         10 | 09          05 | 04          00 |
        +------------------------------------------------------+
        | S  |       B       |       G        |       R        |
        +------------------------------------------------------+

        STP - Transparency control bit (see CLUT)
        R - Red component (5 bits)
        G - Green component(5 bits)
        B - Blue component (5 bits)

    In 32-bit mode:

        | 15                     08 | 07                     00 |
        +-------------------------------------------------------+
        |           G 0             |            R 0            |
        +-------------------------------------------------------+
        |           R 1             |            B 0            |
        +-------------------------------------------------------+
        |           B 1             |            G 1            |
        +-------------------------------------------------------+

        R 0-1: Red component (8bits)
        G 0-1: Green component (8bits)
        B 0-1: Blue component (8bits)

        In 24-bit mode, 3 items of 16-bit data correspond to 2 pixels' worth of data. (R0, G0, B0)
        indicate the pixels on the left, and (R1, R2, B1) indicate the pixels on the right.
    """
    def __init__(self, size, pixel_mode, clut, pos_x, pos_y, width, height, data):
        self.size = size
        self.pixel_mode = pixel_mode
        self.clut = clut
        self.pos_x = pos_x
        self.pos_y = pos_y
        self.width = width
        self.height = height
        self.data = data

    @staticmethod
    def from_file(filename):
        """Read TIM from given file"""
        if not os.path.isfile(filename):
            raise TIMError("Not a file")

        with open(filename, "rb") as source:
            return TIM.from_file_descriptor(source)

    @staticmethod
    def from_file_descriptor(source):
        """Read TIM from given file descriptor"""
        return TIM._from(source, from_bytes=False)

    @staticmethod
    def from_bytes(source, offset=0):
        """Read TIM from given file descriptor"""
        return TIM._from(source, from_bytes=True, offset=offset)

    @staticmethod
    def _from(source, from_bytes=False, offset=0):

        has_clut = False
        pixel_mode = None
        clut = None

        if from_bytes:
            id_version = source[offset:offset + 4]
            offset += 4
        else:
            id_version = source.read(4)

        if id_version != b'\x10\x00\x00\x00':
            raise TIMError(f"Incorrect ID / Version: {id_version}")

        if from_bytes:
            flag = source[offset:offset + 4]
            offset += 4
        else:
            flag = source.read(4)

        flag = int.from_bytes(flag, byteorder='little')
        if flag & 0b1000:
            has_clut = True
        try:
            pixel_mode = TIMPixelMode(flag & 0b0111)
        except ValueError as exc:
            raise TIMError(f"Invalid Pixel Mode: {flag & 0b0111}") from exc

        if pixel_mode in [TIMPixelMode.BIT4_CLUT, TIMPixelMode.BIT8_CLUT]:
            if has_clut:

                if from_bytes:
                    clut_len = source[offset:offset + 4]
                else:
                    clut_len = source.peek(4)[:4]
                clut_len = int.from_bytes(clut_len, byteorder='little')

                if from_bytes:
                    clut_data = source[offset:offset + clut_len]
                    offset += clut_len
                else:
                    clut_data = source.read(clut_len)

                try:
                    clut = CLUT.from_bytes(clut_data)
                except CLUTError as err:
                    raise TIMError("Failed to read CLUT") from err
            else:
                raise TIMError("Pixel Mode is 4-bit or 8-bit CLUT but CLUT Flag not set")
        elif has_clut:
            raise TIMError("CLUT Flag set but Pixel Mode is not 4-bit or 8-bit CLUT")

        if from_bytes:
            bnum = source[offset:offset + 4]
            offset += 4
            pos_x = source[offset:offset + 2]
            offset += 2
            pos_y = source[offset:offset + 2]
            offset += 2
            width = source[offset:offset + 2]
            offset += 2
            height = source[offset:offset + 2]
            offset += 2
        else:
            bnum   = source.read(4)
            pos_x  = source.read(2)
            pos_y  = source.read(2)
            width  = source.read(2)
            height = source.read(2)

        bnum   = int.from_bytes(bnum, byteorder='little')
        pos_x  = int.from_bytes(pos_x, byteorder='little')
        pos_y  = int.from_bytes(pos_y, byteorder='little')
        width  = int.from_bytes(width, byteorder='little')
        height = int.from_bytes(height, byteorder='little')

        size = bnum - PIXEL_DATA_HEADER_SIZE
        expected_size = ((width * height) * 2)

        if size != expected_size:
            raise TIMError(f"Invalid Pixel Data size (expected: {expected_size}, got: {size})")

        if from_bytes:
            data = source[offset:offset + size]
            offset += size
        else:
            data = source.read(size)

        tim = TIM(size, pixel_mode, clut, pos_x, pos_y, width, height, data)

        if from_bytes:
            return (offset, tim)

        return tim

    def render_width(self):
        """Width when rendered"""
        width = self.width
        if self.pixel_mode == TIMPixelMode.BIT4_CLUT:
            width *= 4
        if self.pixel_mode == TIMPixelMode.BIT8_CLUT:
            width *= 2
        if self.pixel_mode == TIMPixelMode.BIT24_DIRECT:
            width *= 2/3

        return int(width)

    def to_format(self, output_format, clut=0, transparency=TIMTransparencyMode.ENABLED):
        """Convert to byte array with given output format"""
        data = bytearray()
        if self.pixel_mode == TIMPixelMode.BIT4_CLUT:
            self._render_from_4bit(output_format, data, clut, transparency)
        elif self.pixel_mode == TIMPixelMode.BIT8_CLUT:
            self._render_from_8bit(output_format, data, clut, transparency)
        elif self.pixel_mode == TIMPixelMode.BIT15_DIRECT:
            self._render_from_15bit(output_format, data, transparency)
        elif self.pixel_mode == TIMPixelMode.BIT24_DIRECT:
            self._render_from_24bit(output_format, data)
        else:
            raise TIMError("Unimplemented for Mixed pixel mode")
        return data

    def _render_from_4bit(self, output_format, output, clut,
                            transparency=TIMTransparencyMode.ENABLED):
        for data in self.data:
            for _ in range(0, 2):
                pix_data = data & 0b1111
                data = data >> 4
                color = self.clut.entries[pix_data + clut*16]

                if output_format == TIMOutputFormat.RGB888:
                    (red, green, blue) = color.to_rgb()
                    output.extend(bytes([red, green, blue]))
                elif output_format == TIMOutputFormat.RGBA8888:
                    (alpha, red, green, blue) = color.to_argb(transparency)
                    output.extend(bytes([red, green, blue, alpha]))
                elif output_format == TIMOutputFormat.BGR888:
                    (red, green, blue) = color.to_rgb()
                    output.extend(bytes([blue, green, red]))

    def _render_from_8bit(self, output_format, output, clut,
                            transparency=TIMTransparencyMode.ENABLED):
        for data in self.data:
            color = self.clut.entries[data + clut*256]

            if output_format == TIMOutputFormat.RGB888:
                (red, green, blue) = color.to_rgb()
                output.extend(bytes([red, green, blue]))
            elif output_format == TIMOutputFormat.RGBA8888:
                (alpha, red, green, blue) = color.to_argb(transparency)
                output.extend(bytes([red, green, blue, alpha]))
            elif output_format == TIMOutputFormat.BGR888:
                (red, green, blue) = color.to_rgb()
                output.extend(bytes([blue, green, red]))

    def _render_from_15bit(self, output_format, output, transparency=TIMTransparencyMode.ENABLED):
        offset = 0
        while offset < len(self.data):
            color = CLUTEntry.from_bytes(self.data[offset:offset + 2])
            offset += 2

            if output_format == TIMOutputFormat.RGB888:
                (red, green, blue) = color.to_rgb()
                output.extend(bytes([red, green, blue]))
            elif output_format == TIMOutputFormat.RGBA8888:
                (alpha, red, green, blue) = color.to_argb(transparency)
                output.extend(bytes([red, green, blue, alpha]))
            elif output_format == TIMOutputFormat.BGR888:
                (red, green, blue) = color.to_rgb()
                output.extend(bytes([blue, green, red]))

    def _render_from_24bit(self, output_format, output):
        offset = 0

        while offset < len(self.data) - 6:
            red0   = self.data[offset]
            green0 = self.data[offset+1]
            blue0  = self.data[offset+2]
            red1   = self.data[offset+3]
            green1 = self.data[offset+4]
            blue1  = self.data[offset+5]

            offset += 6

            if output_format == TIMOutputFormat.RGB888:
                output.extend(bytes([red0, green0, blue0]))
                output.extend(bytes([red1, green1, blue1]))
            elif output_format == TIMOutputFormat.RGBA8888:
                output.extend(bytes([red0, green0, blue0]))
                output.extend(bytes([red1, green1, blue1]))
            elif output_format == TIMOutputFormat.BGR888:
                output.extend(bytes([blue0, green0, red0]))
                output.extend(bytes([blue1, green1, red1]))

class TIMError(Exception):
    """Used to indicate that provided data is invalid TIM data"""

class CLUT:
    """Color LookUp Table

    The CF flag in the FLAG block specifies whether or not the TIM file has a CLUT block. A CLUT is
    a color palette, and is used by image data in 4-bit and 8-bit mode.

    The number of bytes in the CLUT (bnum) is at the top of the CLUT block. This is followed by
    information on its location in the frame buffer, image size, and the substance of the data.

    | 31             00 |
    +-------------------+
    |       bnum        |
    +-------------------+
    |   DY    |  DX     |
    +-------------------+
    |    H    |   W     |
    +-------------------+
    |  CLUT1  | CLUT0   |
    +-------------------+
    |        ...        |
    |        ...        |
    +-------------------+
    |  CLUTn  | CLUTn-1 |
    +-------------------+

    bnum:       Data length of the CLUT block. Units: bytes. Includes the 4 bytes of bnum.
    DX:         x coordinate in frame buffer
    DY:         y coordinate in frame buffer
    H:          Size of data in vertical direction
    W:          Size of data in horizontal direction
    CLUT 1~N:   CLUT entry (16 bits per entry)

    In 4-bit mode, one CLUT consists of 16 CLUT entries. In 8-bit mode, one CLUT consists of 256
    CLUT entries.

    In the PlayStation system, CLUTs are located in the frame buffer, so the CLUT block of a TIM
    file is handled as a rectangular frame buffer image. In other words, one CLUT entry is
    equivalent to one pixel in the frame buffer. In 4-bit mode, one CLUT is handled as an item of
    rectangular image data with a height of 1 and a width of 16; in 8-bit mode, it is handled as an
    item of rectangular image data with a height of 1 and a width of 256.

    One TIM file can hold several CLUTs. In this case, the area in which several CLUTs are combined
    is placed in the CLUT block as a single item of image data.
    """
    def __init__(self, size, pos_x, pos_y, width, height, entries):
        self.size = size
        self.pos_x = pos_x
        self.pos_y = pos_y
        self.width  = width
        self.height  = height
        self.entries = entries

    @staticmethod
    def from_bytes(source):
        """Read CLUT from given bytes"""
        bnum = int.from_bytes(source[0:4], byteorder='little')
        if bnum != len(source):
            raise CLUTError(f"Invalid source data, expected size was {bnum}, got {len(source)}.")

        pos_x  = int.from_bytes(source[4:6],   byteorder='little')
        pos_y  = int.from_bytes(source[6:8],   byteorder='little')
        width  = int.from_bytes(source[8:10],  byteorder='little')
        height = int.from_bytes(source[10:12], byteorder='little')

        size = bnum - CLUT_HEADER_SIZE
        expected_size = ((width * height) * 2)
        if size != expected_size:
            raise CLUTError(f"Invalid CLUT size (expected: {expected_size}, got: {size})")

        entries = []
        offset = CLUT_HEADER_SIZE
        clut_idx = 0
        while offset < len(source):
            entries.append(CLUTEntry.from_bytes(source[offset:offset + 2]))
            clut_idx += 1
            offset = CLUT_HEADER_SIZE + clut_idx*2
        return CLUT(bnum - CLUT_HEADER_SIZE, pos_x, pos_y, width, height, entries)

    def entry_at(self, index, clut_set=0):
        """Get CLUT Entry at specified index for given CLUT set"""
        return self.entries[index + clut_set*self.width]

class CLUTEntry:
    """A CLUT Entry (=one color)

    | 15 | 14         10 | 09          05 | 04          00 |
    +------------------------------------------------------+
    | S  |       B       |       G        |       R        |
    +------------------------------------------------------+

    STP - Transparency control bit
    R - Red component (5 bits)
    G - Green component(5 bits)
    B - Blue component (5 bits)

    The transparency control bit (STP) is valid when data is used as Sprite data or texture data.
    It controls whether or not the relevant pixel, in the Sprite or polygon to be drawn, is
    transparent. If STP is 1, the pixel is a semitransparent color, and if STP is other than 1, the
    pixel is a non-transparent color.

    R, G and B bits control the color components. If they all have the value 0, and STP is also 0,
    the pixel will be a transparent color. If not, it will be a normal color (non-transparent).
    """
    def __init__(self, red, green, blue, stp):
        self.red = red
        self.green = green
        self.blue = blue
        self.transparency = stp

    @staticmethod
    def from_bytes(source):
        """Read CLUT Entry from given bytes"""
        data = int.from_bytes(source, byteorder='little')
        red   =  data & 0b0000000000011111
        green = (data & 0b0000001111100000) >> 5
        blue  = (data & 0b0111110000000000) >> 10
        stp   = (data & 0b1000000000000000) >> 15
        return CLUTEntry(red, green, blue, stp)

    def to_rgb(self):
        """Return an RGB tuple"""
        return (self.red * 8, self.green * 8, self.blue * 8)

    def to_argb(self, transparency_mode=TIMTransparencyMode.ENABLED):
        """Return an ARGB tuple"""
        alpha = 255

        if transparency_mode != TIMTransparencyMode.DISABLED:

            if self.transparency and transparency_mode != TIMTransparencyMode.NO_SEMI_TRANSPARENCY:
                alpha = 128

            if not self.red and not self.green and not self.blue and not self.transparency:
                alpha = 0

        return (alpha, self.red * 8, self.green * 8, self.blue * 8)

class CLUTError(Exception):
    """Used to indicate that provided data is invalid CLUT data"""
